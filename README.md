# postTools

Tools used for generating 'posts', or sections of ```.plan``` files, on my social shell experiment. (https://jmilne.tk).

---

## License

Creative Commons Zero 1.0 Universal.

See [LICENSE](LICENSE) for details, but this is as close to public domain as I can do in this modern age.

See [the CC0 FAQ](https://wiki.creativecommons.org/wiki/CC0_FAQ) for a little breakdown about what is possible under the license, if you have doubts.
