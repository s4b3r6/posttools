PREFIX=/usr/local

.PHONY: install
install:
	mkdir $(PREFIX)/postTools
	cp npost $(PREFIX)/postTools/npost
	cp newpost $(PREFIX)/postTools/newpost
	cp Makefile $(PREFIX)/postTools/Makefile
	cp LICENSE $(PREFIX)/postTools/LICENSE
	ln -s $(PREFIX)/postTools/npost $(PREFIX)/bin/npost
	ln -s $(PREFIX)/postTools/newpost $(PREFIX)/bin/newpost

.PHONY: uninstall
uninstall:
	rm $(PREFIX)/bin/npost
	rm $(PREFIX)/bin/newpost
	rm -rf $(PREFIX)/postTools
